
```sh
flutter run 
flutter run --release

flutter build apk --release
```

cara mendapatkan singingkey/encryptionkey dari aplikasi.
<https://developers.google.com/android/guides/client-auth>

```sh
cd ./android/
./gradlew.bat signingReport
```

cara pake flutter versi beda.
donlot flutter sdk, lalu letakan ke (misal)
`C:\flutter_v2\`

lalu buat file dengan nama `flutter.bat` didalam folder
proyek. kemudian tulis kode batch berikut:

```cmd
@echo off
C:\flutter_v2\bin\flutter.bat %*
```

lalu untuk menjalankan flutter dari dalam proyek,
gunakan perintah (misal):

```bat
./flutter.bat run
./flutter.bat pub get
// dst
```
