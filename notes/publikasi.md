
cara membuat signingkey/encryptionkey untuk melakukan release/publikasi apk.

buka folder `./android/` di android studio.
buka **Build** - **generate signed build/apk** (muncul window dialog)
pilih opsi **APK** - **next** (muncul form)
pada **keystore path** - **create new** (tentukan lokasi file `.jks`)
lalu isi inputs yang dirasa penting.
lalu **next**, kemudian utnuk **signature versions** pilih **v2 full apk release**.
