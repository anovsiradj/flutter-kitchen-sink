
penilaian: **(   /100)**;

## materi belajar resmi
- Pengenalan Flutter **DONE**
- Pengenalan Button, TextField, Label Text **DONE**
- Pengenalan State **DONE**
- Pengenalan ListView **DONE**
- Multipage dan Navigation **DONE**
- Pengenalan BloC **????**
- Koneksi ke Google Firebase **DONE**
- Pembuatan Login dan Logout **HALT**
- Penggunaan REST API **DONE**
- POST ke REST API **WAIT**
- Upload Gambar ke REST API **WAIT**
- Unlimited data untuk ListView (Load More) **DONE**
- Pengenalan Testing dan Bug Fixing **????**

## yang harus dipelajari dan dipahami dalam menggunakan flutter.
- **(   /100)** kuasai pemrograman dart
- **( 51/100)** pahami framework flutter
- **( 49/100)** pahami stateless/stateful widget
- **( 42/100)** pelajari cara pengunaan rest api 

### BLoC
- **(  1/100)** pemahaman
- **( 21/100)** penggunaan

### pelajari firebase
- **(  1/100)** bisa menggunakan firestore
- **( 49/100)** bisa menggunakan realtime-database (rtdb) 
- **( 31/100)** bisa menggunakan autentikasi untuk session

### pelajari sourcecode yang diberikan oleh atasan (1)
- **(   /100)** pahami alur aplikasi dari A sampai Z
- **(   /100)** coba2 menambah fitur baru
- **(   /100)** coba2 merubah fitur yang sudah ada
- **(   /100)** coba2 membuat aplikasi tandingan
- riset cara attack audio
