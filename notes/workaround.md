

jika mengalami kendala, dengan pesan:
```log
Unhandled Exception: MissingPluginException
(No implementation found for method any on channel ${PACKAGE_NAME})
```

itu biasanya karena incompatibilities, tapi masih bisa dipaksakan.

solusi, hapus aplikasi dari hp. lalu eksekusi:
- `flutter clean`
- `flutter pub get`
- `flutter run`

kadang perlu ubah `manifest.xml` juga.
