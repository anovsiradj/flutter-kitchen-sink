
cara menggunakan navigasi, ganti tampilan / pindah laman.
hal tersebut dinamai "route"
<https://flutter.dev/docs/cookbook/navigation/navigation-basics>
<https://flutter.dev/docs/cookbook/navigation/named-routes>

tentang form dan input
<https://fluttercrashcourse.com/blog/realistic-forms-part1>

ada 2 jenis showdialog (alert / prompt),
yaitu alertdialog dan simpledialog.
<https://api.flutter.dev/flutter/material/AlertDialog-class.html>

cara memilih bentuk layout, mau yang column,row,container,dst.
<https://flutter.dev/docs/development/ui/widgets/layout>

daftar nama2 icons untuk `Icon(Icons.${icon_name})`
<https://material.io/resources/icons>

tutorial jelas tentang class constructor 
<https://dart.academy/creating-objects-and-classes-in-dart-and-flutter/>
<https://bezkoder.com/dart-flutter-constructors/>

pengenalan state management (bloc, etc)
<https://flutter.dev/docs/development/data-and-backend/state-mgmt/intro>

referensi yang digunakan uutnuk belajar firebase
<https://medium.com/@tattwei46/flutter-how-to-do-crud-with-firebase-rtdb-ce61e3ce53a>
<https://medium.com/flutter-community/build-a-note-taking-app-with-flutter-firebase-part-i-53816e7a3788>
<https://medium.com/firebase-tips-tricks/how-to-use-firebase-queries-in-flutter-361f21005467>

