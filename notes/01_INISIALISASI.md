
untuk membuat project, sudah dibuatkan oleh vscode,
melalui command palette dengan perintah `flutter: create project`
(jadi ngikut aja).

secara garis besar, saya tidak tau workflow atau konsep yang digunakan
untuk struktur project. semua sudah di-handle oleh IDE (yaitu vscode).

pengetahuan dasar:
- penulisan kode didalam `./lib/`
- penulisan dependecies pada `./pubspec.yaml`
