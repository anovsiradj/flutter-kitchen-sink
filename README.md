
kumpulan riset belajar flutter, mulai dari null.

### persiapan 

- flutter SDK versi `^2`
- vscode

### instalasi

```sh
git clone git@gitlab.com:anovsiradj/flutter-kitchen-sink.git
cd flutter-kitchen-sink
cp .env.default .env
flutter pub get
flutter run
```

### todos

- migrasi soundnullsafety
- mengimplementasikan semua SM (state management)
