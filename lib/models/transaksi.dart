import './user.dart';

class ModelTransaksi {
  static const List<String> JENIS_TRANSAKSI = ['PENERIMAAN', 'PENGELUARAN'];
  int pk;
  ModelUser user;
  String kode; // a.k.a "nama"
  String jenis;
  DateTime tanggal;
  String catatan;
  double kuantitas;
  double nilaiSatuan;
  double nilaiJumlah;

  ModelTransaksi({
    this.kode,
    this.jenis,
    this.tanggal,
    this.catatan,
    this.kuantitas = 1,
    this.nilaiSatuan = 0,
    this.nilaiJumlah,
  }) {
    if (this.nilaiJumlah == null) {
      this.nilaiJumlah = this.kuantitas * this.nilaiSatuan;
    }
  }
}
