class TypicodeTodo {
  final int id;
  final int userId;
  bool completed;
  String title;

  TypicodeTodo({
    this.title,
    this.id,
    this.userId,
    this.completed,
  });

  factory TypicodeTodo.fromJson(Map<String, dynamic> parsedJson) {
    return TypicodeTodo(
      id: parsedJson['id'],
      userId: parsedJson['userId'],
      completed: parsedJson['completed'],
      title: parsedJson['title'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'userId': this.userId,
      'completed': this.completed,
      'title': this.title,
    };
  }
}
