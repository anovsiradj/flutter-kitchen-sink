class Catat {
  String key;
  String content;
  DateTime created = DateTime.now();
  DateTime updated = DateTime.now();

  Catat({this.content, key, created, updated}) {
    if (created is String) created = DateTime.parse(created);
    if (updated is String) updated = DateTime.parse(updated);

    this.key = key;
    this.created = created ?? DateTime.now();
    this.updated = updated ?? DateTime.now();
  }

  factory Catat.fromJson(Map parsedJson) {
    return Catat(
      key: parsedJson['key'],
      content: parsedJson['content'],
      created: parsedJson['created'].toString(),
      updated: parsedJson['updated'].toString(),
    );
  }

  toJson() {
    return {
      'key': key,
      'content': content,
      "created": created.toString(),
      "updated": updated.toString(),
    };
  }
}
