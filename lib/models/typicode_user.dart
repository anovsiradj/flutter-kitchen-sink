class TypicodeUser {
  int id;
  String name;
  String username;
  String email;
  Map<String, dynamic> address;
  String phone;
  String website;
  Map<String, dynamic> company;

  TypicodeUser({
    this.id,
    this.name,
    this.username,
    this.email,
    this.phone,
    this.website,
    this.address,
    this.company,
  });

  factory TypicodeUser.fromJson(Map<String, dynamic> parsedJson) {
    return TypicodeUser(
      id: parsedJson['id'],
      name: parsedJson['name'],
      username: parsedJson['username'],
      email: parsedJson['email'],
      phone: parsedJson['phone'],
      website: parsedJson['website'],
      address: parsedJson['address'],
      company: parsedJson['company'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'name': this.name,
      'username': this.username,
      'email': this.email,
      'phone': this.phone,
      'website': this.website,
      'address': this.address,
      'company': this.company,
    };
  }
}
