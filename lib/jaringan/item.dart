import 'package:flutter/material.dart';

class JaringanItem extends StatefulWidget {
  final dynamic content;
  final moduleTitle = 'Rincian';

  JaringanItem({Key key, this.content}) : super(key: key);

  @override
  JaringanItemState createState() => JaringanItemState();
}

class JaringanItemState extends State<JaringanItem> {
  final expansionTileKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.moduleTitle)),
      body: ListView(children: _makeList(widget.content.toJson())),
    );
  }

  // menjadikan map/object menjadi expansionTile/accordion
  _makeList(Map<String, dynamic> mapped) {
    List<Widget> children = [];
    mapped.forEach((k, v) {
      if (v is Map) {
        children.add(ExpansionTile(
          title: Text(k.toString()),
          children: _makeList(v),
        ));
      } else {
        children.add(ListTile(
          title: Text(v.toString()),
          subtitle: Text(k.toString()),
        ));
      }
    });
    return children;
  }
}
