import 'package:flutter/material.dart';
import '1.dart';
import '2.dart';
import '../func.dart';

class JaringanMain extends StatelessWidget {
  final moduleTitle = 'Jaringan atau Network';

  const JaringanMain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(moduleTitle),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        children: [
          ElevatedButton(
            child: Text('load JSON dari API'),
            onPressed: () => funcGotoNext1(context, () => Jaringan1()),
          ),
          ElevatedButton(
            child: Text('load JSON dari API, infinite-scrolling'),
            onPressed: () => funcGotoNext1(context, () => Jaringan2()),
          ),
        ],
      ),
    );
  }
}
