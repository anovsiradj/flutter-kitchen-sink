/* 
https://flutter.dev/docs/cookbook/networking/fetch-data
https://medium.com/mellow-code-labs/future-builders-along-with-listview-builders-in-your-flutter-app-6656976edeb7
*/

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_ks/func.dart';
import 'package:http/http.dart' as http;
import '../models/typicode_user.dart';
import './item.dart';

class Jaringan1 extends StatefulWidget {
  final moduleTitle = 'jaringan 1';

  Jaringan1({Key key}) : super(key: key);

  @override
  Jaringan1State createState() => Jaringan1State();
}

class Jaringan1State extends State<Jaringan1> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
      ),
      body: FutureBuilder(
        future: load(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return makeList(snapshot.data);
          }
          if (snapshot.hasError) {
            return Text(snapshot.error);
          }

          return Center(child: CircularProgressIndicator(value: null));
        },
      ),
    );
  }

  Future load() async {
    final result = await http.get(Uri.https(
      'jsonplaceholder.typicode.com',
      'users',
    ));
    return List.from(jsonDecode(result.body))
        .map((i) => TypicodeUser.fromJson(i))
        .toList();
  }

  Widget makeList(List<TypicodeUser> list) {
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (context, int index) {
        return makeItem(list[index]);
      },
    );
  }

  Widget makeItem(TypicodeUser item) {
    return ListTile(
      title: Text(item.name),
      subtitle: Text(item.email),
      trailing: IconButton(
        icon: Icon(Icons.account_box),
        onPressed: () {
          funcGotoNext1(context, () => JaringanItem(content: item));
        },
      ),
    );
  }
}
