/* 
https://codinglatte.com/posts/flutter/listview-infinite-scrolling-in-flutter/
*/

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../models/typicode_todo.dart';

class Jaringan2 extends StatefulWidget {
  final moduleTitle = 'Jaringan 2';

  Jaringan2({Key key}) : super(key: key);

  @override
  Jaringan2State createState() => Jaringan2State();
}

class Jaringan2State extends State<Jaringan2> {
  bool pageIsWait = true;
  bool pageHasMore = true;
  int pageCurrent = 1;
  int pageEachResult = 10;
  final pageController = ScrollController();

  List<TypicodeTodo> todos = [];

  @override
  void initState() {
    super.initState();

    pageController.addListener(() {
      if (pageController.position.pixels ==
          pageController.position.maxScrollExtent) {
        if (pageHasMore) {
          loadTodos(pageCurrent);
        }
      }
    });
    loadTodos(pageCurrent);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
      ),
      body: makeList(),
    );
  }

  Widget makeList() {
    return ListView(
      controller: pageController,
      children: <Widget>[
        for (TypicodeTodo todo in todos) makeItem(todo),
        if (pageIsWait) waitView(),
      ],
    );
  }

  Widget makeItem(TypicodeTodo todo) {
    return ListTile(
      title: Text(todo.title),
      subtitle: Text(todo.completed.toString()),
    );
  }

  Widget waitView() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 24),
      child: Center(child: CircularProgressIndicator(value: null)),
    );
  }

  Future loadTodos(int page) async {
    setState(() {
      pageIsWait = true;
    });

    final result = await http.get(Uri.https(
      'jsonplaceholder.typicode.com',
      'todos',
      {'_page': page.toString()},
    ));
    final additions = List.from(jsonDecode(result.body))
        .map((i) => TypicodeTodo.fromJson(i))
        .toList();

    setState(() {
      pageCurrent++;
      pageIsWait = false;
      if (additions.length < pageEachResult) pageHasMore = false;
      todos.addAll(additions);
    });
  }
}
