import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_ks/func.dart';
import 'package:provider/provider.dart';
import '../comp.dart';
import '1/main.dart';
import '2.dart';

class MyFirebaseMain extends StatefulWidget {
  const MyFirebaseMain({Key key}) : super(key: key);

  static const moduleTitle = 'Firebase (Google)';

  @override
  _MyFirebaseMainState createState() => _MyFirebaseMainState();
}

class _MyFirebaseMainState extends State<MyFirebaseMain> {
  FirebaseOptions firebaseOptions;
  FirebaseApp firebaseApp;
  FirebaseAuth firebaseAuth;
  FirebaseAnalytics firebaseAnalytics;
  FirebaseAnalyticsObserver firebaseAnalyticsObserver;

  @override
  void initState() {
    super.initState();
    initAsyncState();
  }

  Future<void> initAsyncState() async {
    firebaseOptions = FirebaseOptions(
      apiKey: env['FIREBASE_API_KEY'],
      appId: env['FIREBASE_APP_ID'],
      messagingSenderId: 'flutter_ks:/firebase',
      projectId: env['FIREBASE_PROJECT_ID'],
      databaseURL: env['FIREBASE_DATABASE_URL'],
    );

    firebaseApp = await Firebase.initializeApp(options: firebaseOptions);

    firebaseAuth = FirebaseAuth.instanceFor(app: firebaseApp);

    firebaseAnalytics = FirebaseAnalytics();
    firebaseAnalyticsObserver =
        FirebaseAnalyticsObserver(analytics: firebaseAnalytics);
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<FirebaseOptions>(create: (_) => firebaseOptions),
        Provider<FirebaseApp>(create: (_) => firebaseApp),
        Provider<FirebaseAuth>(create: (_) => firebaseAuth),
      ],
      child: _Home(),
    );
  }
}

class _Home extends StatelessWidget {
  const _Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firebaseOptions = context.read<FirebaseOptions>();
    print(firebaseOptions);

    return Scaffold(
      appBar: AppBar(
        title: Text(MyFirebaseMain.moduleTitle),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        children: [
          CompLinkButton(
            text: FirebaseTest1.moduleTitle,
            onPressed: () {
              funcGotoNext1(context, () {
                return Provider<FirebaseApp>(
                  create: (_) => context.read<FirebaseApp>(),
                  child: FirebaseTest1(),
                );
              });
            },
          ),
          CompLinkButton(
            text: FirebaseTest2.moduleTitle,
            onPressed: () {
              funcGotoNext1(context, () {
                return Provider<FirebaseAuth>(
                  create: (_) => context.read<FirebaseAuth>(),
                  child: FirebaseTest2(),
                );
              });
            },
          ),
        ],
      ),
    );
  }
}
