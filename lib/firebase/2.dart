import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';

class FirebaseTest2 extends StatefulWidget {
  const FirebaseTest2({Key key}) : super(key: key);

  static const moduleTitle = 'firebase_test_2';

  @override
  _FirebaseTest2State createState() => _FirebaseTest2State();
}

class _FirebaseTest2State extends State<FirebaseTest2> {
  FirebaseAuth firebaseAuth;
  UserCredential userCredential;

  bool isLogged = false;
  String message = 'User is currently signed out!';

  @override
  void initState() {
    super.initState();

    firebaseAuth = context.read<FirebaseAuth>();
    firebaseAuth.authStateChanges().listen((User user) {
      if (user == null) {
        setState(() {
          isLogged = false;
          message = 'User is currently signed out!';
        });
      } else {
        setState(() {
          isLogged = true;
          message = 'User is signed in!';
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(FirebaseTest2.moduleTitle),
      ),
      body: Center(
        child: Column(
          children: [
            Text(message),
            viewAction(),
          ],
        ),
      ),
    );
  }

  Widget viewAction() {
    return isLogged
        ? ElevatedButton(onPressed: logoutAction, child: Text('LogOut'))
        : ElevatedButton(onPressed: loginAction, child: Text('LogIn'));
  }

  void logoutAction() async {
    await firebaseAuth.signOut();
  }

  void loginAction() async {
    userCredential = await firebaseAuth.signInAnonymously();
    print(userCredential);
  }
}
