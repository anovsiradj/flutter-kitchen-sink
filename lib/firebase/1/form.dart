import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ks/func.dart';
import 'package:flutter_ks/models/catat.dart';

class FirebaseTest1Form extends StatefulWidget {
  const FirebaseTest1Form({
    Key key,
    this.fbdbref,
    this.fbdbrefItem,
  }) : super(key: key);

  final DatabaseReference fbdbref;
  final Catat fbdbrefItem;

  @override
  _FirebaseTest1FormState createState() => _FirebaseTest1FormState();
}

class _FirebaseTest1FormState extends State<FirebaseTest1Form> {
  final formKey = GlobalKey<FormState>();
  Catat content;

  @override
  void initState() {
    super.initState();
    content = widget.fbdbrefItem ?? Catat();
  }

  @override
  Widget build(BuildContext context) {
    final blur = funcBlur(context);

    return GestureDetector(
      onTap: blur,
      child: Scaffold(
        appBar: AppBar(
          title: Text('/firebase/1/form'),
        ),
        body: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                initialValue: content.content,
                decoration: InputDecoration(
                  hintText: 'Catatan',
                ),
                onSaved: (data) {
                  content.content = data;
                },
                validator: (data) {
                  if (data.trim() == '') return 'Tidak boleh kosong.';
                  return null;
                },
              ),
              ElevatedButton(
                child: Text('simpan'),
                onPressed: () {
                  blur();
                  if (formKey.currentState.validate()) {
                    formKey.currentState.save();
                    submit();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> submit() async {
    await widget.fbdbref.push().set(content.toJson());
    Future.delayed(Duration(milliseconds: 100), () {
      Navigator.pop(context, true);
    });
  }
}
