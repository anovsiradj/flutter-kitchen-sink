import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:provider/provider.dart';

import '../../func.dart';
import '../../models/catat.dart';
import './form.dart';

class FirebaseTest1 extends StatefulWidget {
  FirebaseTest1({Key key}) : super(key: key);

  static final moduleTitle = 'firebase_test_1';

  @override
  _FirebaseTest1State createState() => _FirebaseTest1State();
}

class _FirebaseTest1State extends State<FirebaseTest1> {
  FirebaseApp firebaseApp;
  FirebaseDatabase firebaseDatabase;
  DatabaseReference fbDbRefNotes;

  @override
  void initState() {
    super.initState();
    initAsyncState();

    firebaseApp = context.read<FirebaseApp>();

    firebaseDatabase = FirebaseDatabase(app: firebaseApp);
    fbDbRefNotes = firebaseDatabase.reference().child('/notes');
  }

  void initAsyncState() async {
    await funcDateTimeLocaleID();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('/firebase/1/main'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => viewForm(),
      ),
      body: FirebaseAnimatedList(
        query: fbDbRefNotes,
        itemBuilder: (context, snapshot, animation, index) {
          return SizeTransition(
            sizeFactor: animation,
            child: ListTile(
              trailing: Wrap(
                children: [
                  IconButton(
                    onPressed: () => fbDbRefNotes.child(snapshot.key).remove(),
                    icon: const Icon(Icons.delete),
                  ),
                  IconButton(
                    onPressed: () => viewForm(Catat.fromJson(snapshot.value)),
                    icon: const Icon(Icons.edit),
                  ),
                ],
              ),
              title: Text('$index: ${snapshot.value.toString()}'),
              subtitle: Text(funcDateTimeFormatID(snapshot.value['updated'])),
            ),
          );
        },
      ),
    );
  }

  void viewForm([item]) {
    funcGotoNext1(context, () {
      return Provider<DatabaseReference>(
        create: (_) => fbDbRefNotes,
        child: FirebaseTest1Form(
          fbdbref: fbDbRefNotes,
          fbdbrefItem: item,
        ),
      );
    });
  }
}
