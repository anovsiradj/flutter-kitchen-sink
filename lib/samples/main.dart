import 'package:flutter/material.dart';

import '../func.dart';

import '2/a.dart';
import 'firebase_rtdb.dart';
import './shared_preferences.dart';

class SamplesMain extends StatelessWidget {
  final moduleTitle = 'Contoh-Contoh';

  const SamplesMain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(moduleTitle),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        children: [
          ElevatedButton(
            child: Text('/samples/2/a (flutter_plugin_record)'),
            onPressed: () => funcGotoNext1(context, () => Sample2a()),
          ),
          ElevatedButton(
            child: Text('/samples/firebase_rtdb'),
            onPressed: () => funcGotoNext1(context, () => FirebaseRTDB()),
          ),
          ElevatedButton(
            child: Text('/samples/shared_preferences'),
            onPressed: () => funcGotoNext1(context, () => SharPref()),
          ),
        ],
      ),
    );
  }
}
