/*original
<https://github.com/yxwandroid/flutter_plugin_record/blob/master/example/lib/record_screen.dart>

modified
*/

import 'package:flutter/material.dart';
import 'package:flustars/flustars.dart';
import 'package:flutter_plugin_record/index.dart';
import 'package:path_provider/path_provider.dart';

class Sample2a extends StatefulWidget {
  @override
  _Sample2aState createState() => _Sample2aState();
}

class _Sample2aState extends State<Sample2a> {
  FlutterPluginRecord recordPlugin = FlutterPluginRecord();

  String filePath = "";

  @override
  void initState() {
    super.initState();

    ///初始化方法的监听
    recordPlugin.responseFromInit.listen((data) {
      if (data) {
        print("初始化成功");
      } else {
        print("初始化失败");
      }
    });

    /// 开始录制或结束录制的监听
    recordPlugin.response.listen((data) {
      if (data.msg == "onStop") {
        ///结束录制时会返回录制文件的地址方便上传服务器
        print("onStop  文件路径" + data.path);
        filePath = data.path;
        print("onStop  时长 " + data.audioTimeLength.toString());
      } else if (data.msg == "onStart") {
        print("onStart --");
      } else {
        print("--" + data.msg);
      }
    });

    ///录制过程监听录制的声音的大小 方便做语音动画显示图片的样式
    recordPlugin.responseFromAmplitude.listen((data) {
      var voiceData = double.parse(data.msg);
      print("振幅大小   " + voiceData.toString());
    });

    recordPlugin.responsePlayStateController.listen((data) {
      print("播放路径   " + data.playPath);
      print("播放状态   " + data.playState);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('录制wav'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            TextButton(
              child: Text("初始化 _init"),
              onPressed: () {
                _init();
              },
            ),
            TextButton(
              child: Text("开始录制 start"),
              onPressed: () {
                start();
              },
            ),
            TextButton(
              child: Text("根据路径录制wav文件 _requestAppDocumentsDirectory"),
              onPressed: () {
                _requestAppDocumentsDirectory();
              },
            ),
            TextButton(
              child: Text("停止录制 stop"),
              onPressed: () {
                stop();
              },
            ),
            TextButton(
              child: Text("播放 play"),
              onPressed: () {
                play();
              },
            ),
            TextButton(
              child: Text("播放本地指定路径录音文件 playByPath"),
              onPressed: () {
                print(filePath);
                playByPath(filePath, "file");
              },
            ),
            TextButton(
              child: Text("播放网络wav文件 playByPath:http"),
              onPressed: () {
                playByPath(
                    "https://upload.wikimedia.org/wikipedia/commons/4/40/Dogga.wav",
                    "url");
              },
            ),
            TextButton(
              child: Text("暂停|继续播放 pause"),
              onPressed: () {
                pause();
              },
            ),
            TextButton(
              child: Text("停止播放 stopPlay"),
              onPressed: () {
                stopPlay();
              },
            ),
          ],
        ),
      ),
    );
  }

  void _requestAppDocumentsDirectory() {
//    if(Platform.isIOS){
//      //ios相关代码
//      setState(() {
//        getApplicationDocumentsDirectory().then((value) {
//          String nowDataTimeStr = DateUtil.getNowDateMs().toString();
//          String wavPath = value.path + "/" + nowDataTimeStr + ".wav";
//          startByWavPath(wavPath);
//        });
//      });
//    }else if(Platform.isAndroid){
//      //android相关代码
//    }

    setState(() {
      getApplicationDocumentsDirectory().then((value) {
        String nowDataTimeStr = DateUtil.getNowDateMs().toString();
        String wavPath = value.path + "/" + nowDataTimeStr + ".wav";
        print(wavPath);
        startByWavPath(wavPath);
      });
    });
  }

  ///初始化语音录制的方法
  void _init() async {
    await recordPlugin.init();
  }

  ///开始语音录制的方法
  void start() async {
    await recordPlugin.start();
  }

  ///根据传递的路径进行语音录制
  void startByWavPath(String wavPath) async {
    await recordPlugin.startByWavPath(wavPath);
  }

  ///停止语音录制的方法
  void stop() async {
    await recordPlugin.stop();
  }

  ///播放语音的方法
  void play() async {
    await recordPlugin.play();
  }

  ///播放指定路径录音文件  url为iOS播放网络语音，file为播放本地语音文件
  void playByPath(String path, String type) async {
    await recordPlugin.playByPath(path, type);
  }

  ///暂停|继续播放
  void pause() async {
    await recordPlugin.pausePlay();
  }

  @override
  void dispose() {
    /// 当界面退出的时候是释放录音资源
    recordPlugin.dispose();
    super.dispose();
  }

  void stopPlay() async {
    await recordPlugin.stopPlay();
  }
}
