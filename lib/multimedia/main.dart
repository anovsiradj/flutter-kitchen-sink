import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_ks/multimedia/audio.dart';
import 'package:flutter_ks/multimedia/audio_recorder.dart';
import 'package:flutter_ks/multimedia/video.dart';
import 'package:mime/mime.dart';

/*
  https://developer.android.com/guide/topics/media/media-formats
*/
const List<String> defaultImageExtensions = [
  'jpg',
  'jpeg',
  'png',
  'gif',
  'bmp',
];
const List<String> defaultVideoExtensions = [
  '3gp',
  'mkv',
  'mp4',
  'avi',
];
const List<String> defaultAudioExtensions = [
  'flac',
  'amr',
  'm4a',
  'mp3',
  'wav',
];
final List<String> defaultExtensions = [
      'oga',
      'ogg',
      'ogv',
      'webm',
      'webp',
    ] +
    defaultImageExtensions +
    defaultAudioExtensions +
    defaultVideoExtensions;

class MultimediaMain extends StatefulWidget {
  final moduleTitle = 'Multimedia Viewer/Player';

  const MultimediaMain({Key key}) : super(key: key);

  @override
  State<MultimediaMain> createState() => _MultimediaMainState();
}

class _MultimediaMainState extends State<MultimediaMain> {
  String mediaPath;
  String mediaMime;

  MediaAudioRecorder _audioRecorder;

  @override
  Widget build(BuildContext context) {
    final _browseFile = IconButton(
      icon: const Icon(Icons.search),
      onPressed: browseFile,
    );
    final _takePhoto = IconButton(
      icon: const Icon(Icons.photo_camera),
      onPressed: takePhoto,
    );
    final _takeVideo = IconButton(
      icon: const Icon(Icons.videocam),
      onPressed: takeVideo,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
        actions: [_browseFile, _takePhoto, _takeVideo, takeAudio()],
      ),
      body: Center(child: displayMedia()),
    );
  }

  void takeAudioCallback(String path, String mime) {
    setState(() {});
  }

  Future<void> resetMedia() async {
    setState(() {
      if (_audioRecorder != null) {
        _audioRecorder = null;
      }
      mediaPath = null;
      mediaMime = null;
    });
    await Future.delayed(Duration(milliseconds: 0));
  }

  void browseFile() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: defaultExtensions,
    );
    if (result != null) {
      await resetMedia();
      setState(() {
        mediaPath = result.files.single.path;
        mediaMime = lookupMimeType(mediaPath);
      });
    }
  }

  Widget takeAudio() {
    return IconButton(
      icon: const Icon(Icons.record_voice_over),
      onPressed: () async {
        await resetMedia();
        setState(() {
          _audioRecorder = MediaAudioRecorder(
            recordedCallback: takeAudioCallback,
          );
        });
      },
    );
  }

  void takePhoto() async {
    final _picker = ImagePicker();
    final result = await _picker.pickImage(source: ImageSource.camera);
    if (result != null) {
      await resetMedia();
      setState(() {
        mediaPath = result.path;
        mediaMime = result.mimeType;
      });
    }
  }

  void takeVideo() async {
    final _picker = ImagePicker();
    final result = await _picker.pickVideo(source: ImageSource.camera);
    if (result != null) {
      await resetMedia();
      setState(() {
        mediaPath = result.path;
        mediaMime = result.mimeType;
      });
    }
  }

  Widget displayMedia() {
    if (_audioRecorder != null) {
      return _audioRecorder;
    }
    if (mediaPath != null) {
      if ((mediaMime != null && mediaMime.contains('image/')) ||
          defaultImageExtensions
              .any((i) => mediaPath.toLowerCase().endsWith(i))) {
        return Image.file(File(mediaPath));
      }
      if ((mediaMime != null && mediaMime.contains('video/')) ||
          defaultVideoExtensions
              .any((i) => mediaPath.toLowerCase().endsWith(i))) {
        return MediaVideo(href: mediaPath);
      }
      if ((mediaMime != null && mediaMime.contains('audio/')) ||
          defaultAudioExtensions
              .any((i) => mediaPath.toLowerCase().endsWith(i))) {
        return MediaAudio(href: mediaPath);
      }
      return const Text('tidak bisa menjalankan/menampilkan media');
    }
    return Text(widget.moduleTitle);
  }
}
