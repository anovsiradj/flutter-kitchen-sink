import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_ks/multimedia/audio.dart';
import 'package:record/record.dart';

class MediaAudioRecorder extends StatefulWidget {
  final void Function(String path, String mime) recordedCallback;
  const MediaAudioRecorder({Key key, this.recordedCallback}) : super(key: key);

  @override
  State<MediaAudioRecorder> createState() => _MediaAudioRecorderState();
}

class _MediaAudioRecorderState extends State<MediaAudioRecorder> {
  Record _recorder;
  final _permission = StreamController<bool>.broadcast();
  final _recording = StreamController<bool>.broadcast();

  String filePath;
  final String fileMime = 'audio/aac'; // default android
  final AudioEncoder fileEncoder = AudioEncoder.AAC; // default android

  @override
  void initState() {
    super.initState();

    _recorder = Record();
    _permission.add(false);
    _recording.add(false);
    initAsyncState();
  }

  void initAsyncState() async {
    _permission.add(await _recorder.hasPermission());

    _recording.stream.listen((data) async {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _recorder.dispose();
    _permission.close();
    _recording.close();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        StreamBuilder(
          stream: _permission.stream,
          builder: (contextControls, snapshot) {
            if (snapshot.hasData && snapshot.data) return controls();
            return const Text('tidak bisa melakukan perekaman');
          },
        ),
        filePath != null ? MediaAudio(href: filePath) : SizedBox(height: 0),
      ],
    );
  }

  Widget controls() {
    return StreamBuilder(
      stream: _recording.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data) {
          return IconButton(
            icon: Icon(Icons.stop),
            onPressed: () async {
              filePath = await _recorder.stop();
              _recording.add(await _recorder.isRecording());
            },
          );
        } else {
          return IconButton(
            icon: Icon(Icons.play_arrow),
            onPressed: () async {
              filePath = null;
              await _recorder.start(
                path: filePath,
                encoder: AudioEncoder.AAC,
              );
              _recording.add(await _recorder.isRecording());
            },
          );
        }
      },
    );
  }
}
