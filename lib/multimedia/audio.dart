import 'package:flutter/material.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:path/path.dart';

class MediaAudio extends StatelessWidget {
  final String href;

  MediaAudio({Key key, this.href}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
      child: Column(
        children: [
          SoundPlayerUI.fromLoader(
            _track,
            showTitle: false,
            audioFocus: AudioFocus.requestFocusAndDuckOthers,
            backgroundColor: Colors.transparent,
          ),
        ],
      ),
    );
  }

  Future<Track> _track(BuildContext context) async {
    return Track(
      trackPath: href,
      trackTitle: basename(href),
      codec: Codec.defaultCodec,
    );
  }
}
