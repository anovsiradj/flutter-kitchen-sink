/*
sumber langsung berasal dari pengembang
https://github.com/flutter/plugins/blob/master/packages/video_player/video_player/example/lib/main.dart
*/

import 'dart:async';
import 'dart:io';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class MediaVideo extends StatefulWidget {
  const MediaVideo({Key key, @required this.href}) : super(key: key);

  final String href;

  @override
  _MediaVideoState createState() => _MediaVideoState();
}

class _MediaVideoState extends State<MediaVideo> {
  VideoPlayerController _controller;
  bool isOpacity = true;
  Timer hideTimer;

  @override
  void initState() {
    super.initState();

    _controller = VideoPlayerController.file(
      File(widget.href),
      videoPlayerOptions: VideoPlayerOptions(
        mixWithOthers: true,
      ),
    );
    _controller.setLooping(true);
    _controller.addListener(() => setState(() {}));
    _controller.initialize().then((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    if (_controller.value.isInitialized) {
      return Container(
        child: AspectRatio(
          aspectRatio: _controller.value.aspectRatio,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              VideoPlayer(_controller),
              controlButton(),
              controlHandle(),
              VideoProgressIndicator(_controller, allowScrubbing: true),
            ],
          ),
        ),
      );
    }

    return Center(child: CircularProgressIndicator(value: null));
  }

  Widget controlButton() {
    return AnimatedOpacity(
      opacity: isOpacity ? 1 : 0,
      duration: Duration(milliseconds: 500),
      child: Container(
        color: Colors.black38,
        alignment: Alignment.center,
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
          color: Colors.white,
          size: 100,
        ),
      ),
    );
  }

  Widget controlHandle() {
    return GestureDetector(
      onTap: () {
        // nek ndelik, ketokke
        if (!isOpacity) {
          setState(() => isOpacity = !isOpacity);
        }

        // pateni timer menawa urip
        if (hideTimer?.isActive ?? false) hideTimer.cancel();

        hideTimer = Timer(const Duration(milliseconds: 1000), () {
          // nek ketok, delikke
          if (isOpacity) {
            setState(() => isOpacity = !isOpacity);
          }
        });

        _controller.value.isPlaying ? _controller.pause() : _controller.play();
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    hideTimer?.cancel();
    _controller?.dispose();
  }
}
