/*
implementasi pertama dari pemahaman saya tentang bloc.

kenapa pake typedef a.k.a <T>, karena dengan itu, kita bisa
passing typedef kedalam input maupun output dari functions
pada class.

kenapa pake `StreamController.broadcast()` dan tidak 
`StreamController()` saja, karena supaya bisa pake 
`instance.stream` berkali2.
*/

import 'dart:async';

class Bloc1<T> {
  T initialData;
  final _controller = StreamController<T>.broadcast();

  Stream<T> get stream => _controller.stream;
  void sink(T data) => _controller.sink.add(data);

  Bloc1([this.initialData]) {
    if (this.initialData != null) {
      this.sink(this.initialData);
    }
  }

  void dispose() {
    _controller.close();
  }
}
