import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:mime/mime.dart';

import 'video.dart';

class BelajarMediaViewer extends StatefulWidget {
  BelajarMediaViewer({Key key}) : super(key: key);

  final moduleTitle = 'Media Viewer';

  @override
  BelajarMediaViewerState createState() => BelajarMediaViewerState();
}

class BelajarMediaViewerState extends State<BelajarMediaViewer> {
  String selectedFilePath;
  String selectedFileMime;

  @override
  Widget build(BuildContext context) {
    final browseMenu = PopupMenuButton<int>(
      onSelected: (result) {
        if (result == 0) resetSelected();
        FileType selectedType;
        if (result == 1) selectedType = FileType.media;
        if (result == 2) selectedType = FileType.video;
        if (result == 3) selectedType = FileType.image;
        if (selectedType != null) {
          browseFile(selectedType);
        }
      },
      itemBuilder: (BuildContext context) => [
        const PopupMenuItem(value: 1, child: Text('Pilih media ...')),
        const PopupMenuItem(value: 2, child: Text('Pilih video ...')),
        const PopupMenuItem(value: 3, child: Text('Pilih image ...')),
        const PopupMenuItem(value: 0, child: Text('Reset')),
      ],
    );

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(widget.moduleTitle),
          actions: [browseMenu],
        ),
        body: Center(
          child: serveFile(),
        ),
      ),
    );
  }

  Future<void> resetSelected() async {
    setState(() {
      selectedFilePath = null;
      selectedFileMime = null;
    });
    await Future.delayed(Duration(milliseconds: 100));
  }

  void browseFile(type) async {
    final result = await FilePicker.platform.pickFiles(
      type: type,
    );
    if (result != null) {
      await resetSelected();
      setState(() {
        selectedFilePath = result.files.single.path;
        selectedFileMime = lookupMimeType(selectedFilePath);
      });
    }
  }

  Widget serveFile() {
    if (selectedFilePath != null) {
      if (selectedFileMime.contains('image/')) {
        return Image.file(File(selectedFilePath));
      }
      if (selectedFileMime.contains('video/')) {
        return MediaViewerVideo(href: selectedFilePath);
      }
    }
    return Text(widget.moduleTitle);
  }
}
