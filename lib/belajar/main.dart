import 'package:flutter/material.dart';
import '../comp.dart';
import '3/main.dart';
import '2.dart';
import '2b.dart';
import '1/main.dart';
import '../func.dart';
import 'media_viewer/main.dart';

class BelajarMain extends StatelessWidget {
  final moduleTitle = 'Belajar untuk JobDesk';

  const BelajarMain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(moduleTitle),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        children: [
          CompLinkButton(
            text: 'rekam suara',
            onPressed: () => funcGotoNext1(context, () => Belajar3()),
          ),
          CompLinkButton(
            text: 'dynamic dropdown/combobox pada form',
            onPressed: () => funcGotoNext1(context, () => Belajar2()),
          ),
          CompLinkButton(
            text: 'dynamic dropdown/combobox pada form (b)',
            onPressed: () => funcGotoNext1(context, () => Belajar2b()),
          ),
          CompLinkButton(
            text: 'belajar 1',
            onPressed: () => funcGotoNext1(context, () => Belajar1App()),
          ),
          CompLinkButton(
            text: 'buat image dan video viewer',
            onPressed: () => funcGotoNext1(context, () => BelajarMediaViewer()),
          ),
        ],
      ),
    );
  }
}
