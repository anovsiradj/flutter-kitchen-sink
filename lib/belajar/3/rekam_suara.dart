import 'dart:async';

import 'package:flutter/material.dart';

/* package(s) untuk melakukan rekam suara */
import 'package:flutter_plugin_record/flutter_plugin_record.dart';

import '../../bloc/1.dart';

class Belajar3RekamSuara extends StatefulWidget {
  final moduleTitle = 'Rekam Suara';
  @override
  _Belajar3RekamSuaraState createState() => _Belajar3RekamSuaraState();
}

class _Belajar3RekamSuaraState extends State<Belajar3RekamSuara> {
  final recordPlugin = FlutterPluginRecord();

  final durasiDetik = 60; // 60 detik = 1 menit
  final durasiRedrawMilidetik = 250; // 1000 milidetik = 1 detik

  final durasiSW = Stopwatch();
  final isRekam = Bloc1<bool>(false);
  String pathResult;

  @override
  void initState() {
    super.initState();
    initAsyncState();

    isRekam.stream.listen((data) async {
      print(pathResult);
      if (data) {
        durasiSW.reset();
        durasiSW.start();
        await recordPlugin.start();
      } else {
        durasiSW.stop();
        await recordPlugin.stop();
      }
    });

    recordPlugin.response.listen((data) {
      if (data.msg == "onStop") {
        // print("onStop " + data.path);

        setState(() {
          pathResult = data.path;
        });
        // print("onStop  -- " + data.audioTimeLength.toString());
      } else if (data.msg == "onStart") {
        // print("onStart -- ");
      } else {
        // print("--" + data.msg);
        print(data.toString());
      }
    });
  }

  void initAsyncState() async {
    await recordPlugin.initRecordMp3();
  }

  @override
  void dispose() {
    super.dispose();
    isRekam.dispose();
    recordPlugin.dispose();
  }

  Stream<int> durasiState() {
    Stream<int> state = Stream<int>.periodic(
      Duration(milliseconds: durasiRedrawMilidetik),
      (computation) => computation,
    );
    return state.take((1000 * durasiDetik) ~/ durasiRedrawMilidetik);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
        actions: [
          IconButton(
            icon: Icon(Icons.check),
            onPressed: pathResult == null
                ? null
                : () {
                    Navigator.pop(context, pathResult);
                  },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: buildContent(context),
      ),
    );
  }

  Widget buildContent(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        buttonGroupManualRekam(),
        StreamBuilder(
          stream: isRekam.stream,
          initialData: isRekam.initialData,
          builder: (context, isRekamSnapshot) {
            return StreamBuilder(
              stream: isRekamSnapshot.data ? durasiState() : null,
              builder: (context, snapshot) {
                final menit = durasiSW.elapsed.inMinutes % 60;
                final detik = durasiSW.elapsed.inSeconds % 60;
                final milidetik = durasiSW.elapsedMilliseconds % 1000;

                if (snapshot.connectionState == ConnectionState.done) {
                  isRekam.sink(false);
                }

                return Container(
                  alignment: Alignment.center,
                  child: Text(
                    '${menit.toString().padLeft(2, '0')}.'
                    '${detik.toString().padLeft(2, '0')}.'
                    '${milidetik.toString().padLeft(3, '0')}',
                    style: TextStyle(
                      fontSize: 30.0,
                    ),
                  ),
                );
              },
            );
          },
        ),
        buttonJumboRekam(),
      ],
    );
  }

  Widget buttonJumboRekam() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.75,
      child: FittedBox(
        child: StreamBuilder(
          stream: isRekam.stream,
          initialData: isRekam.initialData,
          builder: (context, snapshot) {
            return IconButton(
              padding: EdgeInsets.all(0),
              icon: Icon(snapshot.data ? Icons.stop : Icons.play_arrow),
              onPressed: () {
                isRekam.sink(!snapshot.data);
              },
            );
          },
        ),
      ),
    );
  }

  Widget buttonGroupManualRekam() {
    return Row(
      children: [
        TextButton(
          child: Text("memulai"),
          onPressed: () {
            start();
          },
        ),
        TextButton(
          child: Text("berhenti"),
          onPressed: () {
            stop();
          },
        ),
        TextButton(
          child: Text("memainkan"),
          onPressed: () {
            play();
          },
        ),
      ],
    );
  }

  void start() async {
    await recordPlugin.start();
  }

  void stop() async {
    await recordPlugin.stop();
  }

  void play() async {
    await recordPlugin.play();
  }
}
