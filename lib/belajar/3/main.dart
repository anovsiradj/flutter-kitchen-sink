import 'dart:async';

import 'package:flutter/material.dart';

/* package(s) untuk melakukan browse suara */
import 'package:file_picker/file_picker.dart';

import '../../func.dart';
import '../../bloc/1.dart';
import './rekam_suara.dart';

class Belajar3 extends StatelessWidget {
  Belajar3({Key key}) : super(key: key);

  final moduleTitle = 'Rekam Suara';
  final pathResult = Bloc1<String>();

  void dispose() {
    pathResult.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(moduleTitle),
      ),
      body: Column(
        children: [
          StreamBuilder(
            stream: pathResult.stream,
            builder: (context, snapshot) {
              final style = TextButton.styleFrom(
                backgroundColor:
                    snapshot.hasData ? Colors.red[600] : Colors.green[600],
              );
              return ElevatedButton(
                style: style,
                child: Text(
                  snapshot.hasData ? 'Batal pilih suara' : 'Pilih suara',
                ),
                onPressed: () {
                  if (snapshot.hasData) {
                    pathResult.sink(null);
                  } else {
                    viewDialogSuara(context);
                  }
                },
              );
            },
          ),
          StreamBuilder(
            stream: pathResult.stream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data.toString());
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }

  Future<void> viewDialogSuara(BuildContext context) async {
    final padding = EdgeInsets.symmetric(horizontal: 4);
    final opsi = await showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return SimpleDialog(
          title: Text('Suara'),
          children: [
            SimpleDialogOption(
              onPressed: () => funcGotoPrev1(dialogContext, () => 1),
              child: Row(
                children: [
                  Padding(child: Icon(Icons.file_present), padding: padding),
                  Text('Ambil suara dari penyimpanan'),
                ],
              ),
            ),
            SimpleDialogOption(
              onPressed: () => funcGotoPrev1(dialogContext, () => 2),
              child: Row(
                children: [
                  Padding(
                      child: Icon(Icons.record_voice_over), padding: padding),
                  Text('Rekam suara secara langsung'),
                ],
              ),
            ),
          ],
        );
      },
    );
    if (opsi == 1) {
      await loadSuaraFromStorage();
    }
    if (opsi == 2) {
      final result = await funcGotoNext1(context, () => Belajar3RekamSuara());
      if (result is String) {
        pathResult.sink(result);
      }
    }
  }

  Future<void> loadSuaraFromStorage() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.audio,
    );

    if (result != null) {
      pathResult.sink(result.files.single.path);
    }
  }
}
