/*
# DONE

sama seperti `./2.dart` hanya saja menggunakan plugin tertentu.
yaitu `groovin_widgets:OutlineDropdownButtonFormField`.

widget mengharuskan untuk atur attr:value, jika tidak
tampilan selctedValue tidak akan terlihat (blank).
*/

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:groovin_widgets/groovin_widgets.dart';

class Belajar2b extends StatefulWidget {
  final moduleTitle = 'Belajar 2b';

  Belajar2b({Key key}) : super(key: key);

  @override
  Belajar2bState createState() => Belajar2bState();
}

class Belajar2bState extends State<Belajar2b> {
  List albums = [];
  List photos = [];

  Map selectedAlbum;
  Map selectedPhoto;
  @override
  void initState() {
    super.initState();
    loadAlbums();
  }

  void loadAlbums() async {
    setState(() {
      albums.clear();
    });
    final httpResult = await http.get(Uri.http(
      'jsonplaceholder.typicode.com',
      'albums',
      {'_page': 1.toString()},
    ));
    setState(() {
      albums.addAll(jsonDecode(httpResult.body));
    });
  }

  void loadPhotos(albumId) async {
    setState(() {
      photos.clear();
    });
    final httpResult = await http.get(Uri.http(
      'jsonplaceholder.typicode.com',
      'photos',
      {
        '_page': 1.toString(),
        'albumId': albumId.toString(),
      },
    ));
    setState(() {
      photos.addAll(jsonDecode(httpResult.body));
      selectedPhoto = null; /* MUST */
    });
  }

  Widget dropdownAlbums() {
    return OutlineDropdownButtonFormField(
      hint: Text('album ...'),
      onChanged: (data) {
        setState(() {
          selectedAlbum = data; /* MUST */
        });
        loadPhotos(data['id']);
      },
      value: selectedAlbum,
      items: albums
          .map(
            (i) => DropdownMenuItem(
              child: Text(i['title']),
              value: i,
            ),
          )
          .toList(),
    );
  }

  Widget dropdownPhotos() {
    return OutlineDropdownButtonFormField(
      hint: Text('photos ...'),
      onChanged: (data) {
        setState(() {
          selectedPhoto = data; /* MUST */
        });
      },
      value: selectedPhoto,
      items: photos
          .map(
            (i) => DropdownMenuItem(
              child: Text(i['title']),
              value: i,
            ),
          )
          .toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets _padding = EdgeInsets.all(8);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
      ),
      body: Form(
        child: Column(
          children: [
            Padding(padding: _padding, child: dropdownAlbums()),
            Padding(padding: _padding, child: dropdownPhotos()),
            Padding(padding: _padding, child: Text(selectedPhoto.toString())),
            SizedBox(height: 12),
            if (selectedPhoto != null)
              Image.network(selectedPhoto['thumbnailUrl']),
          ],
        ),
      ),
    );
  }
}
