import 'dart:async';

class MappedBloc {
  final StreamController<Map<String, dynamic>> dataController =
      StreamController<Map<String, dynamic>>.broadcast();

  void update(Map<String, dynamic> content) {
    dataController.sink.add(content);
  }

  void dispose() {
    dataController.close();
  }
}
