import 'dart:async';

class DataBloc {
  final StreamController<String> dataController =
      StreamController<String>.broadcast();

  void update(String idData) {
    dataController.sink.add(idData);
  }

  void dispose() {
    dataController.close();
  }
}

final dataBloc = DataBloc();
