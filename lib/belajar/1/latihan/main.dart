import 'package:flutter/material.dart';
import '../bloc/mapped_bloc.dart';

class Belajar1L extends StatefulWidget {
  Belajar1L({Key key}) : super(key: key);

  @override
  _Belajar1LState createState() => _Belajar1LState();
}

class _Belajar1LState extends State<Belajar1L> {
  final moduleTitle = 'belajar_1_latihan';
  final bloc = MappedBloc();
  List contents = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: moduleTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(moduleTitle),
        ),
        body: mainLayout(),
      ),
    );
  }

  Widget mainLayout() {
    return ListView.builder(
      itemCount: contents.length,
      itemBuilder: makeListItem,
    );
  }

  Widget makeListItem(BuildContext context, int i) {
    return ListTile(
      title: Text(contents[i]),
    );
  }
}
