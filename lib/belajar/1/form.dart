import 'package:flutter/material.dart';
import './bloc/data_bloc.dart';

class Belajar1Form extends StatefulWidget {
  final moduleTitle = 'Belajar1Form';

  @override
  State<StatefulWidget> createState() => Belajar1FormState();
}

class Belajar1FormState extends State<Belajar1Form> {
  void save() {
    dataBloc.update('dari bloc');
    Navigator.pop(context, 'dari navi');
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.moduleTitle)),
      floatingActionButton: FloatingActionButton(
        onPressed: save,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
