/*
# DONE

sourcecode dari atasan.
contoh2 navigasi, bloc, input dan output routes.
*/

import 'dart:async';
import 'package:flutter/material.dart';
import './bloc/data_bloc.dart';
import './form.dart';
import './atas.dart';
import './bawah.dart';

class Belajar1App extends StatefulWidget {
  Belajar1App({Key key}) : super(key: key);

  @override
  _Belajar1AppState createState() => _Belajar1AppState();
}

class _Belajar1AppState extends State<Belajar1App> {
  String moduleTitle = 'Belajar1App';

  void actionOnPressed() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Belajar1Form(),
      ),
    );
    print(result);
  }

  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    subscription = dataBloc.dataController.stream.listen((data) {
      setState(() {
        moduleTitle = data;
        print(moduleTitle);
      });
    });
  }

  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => actionOnPressed(),
      ),
      appBar: AppBar(title: Text(moduleTitle)),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Atas(),
            Bawah(),
          ],
        ),
      ),
    );
  }
}
