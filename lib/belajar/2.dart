/*
# DONE

harus diingat, ketika ingin perbarui values. waktu meng-null-kan selectedValue 
harus "sync" (bukan async) setelah values  sudah diganti dengan yang baru. 
metode-metodenya diantaranya sebagai berikut.

```
// 1
await loadValues();
selectedValue = null;

// 2 (callback)
loadValues(() {
  selectedValue = null;
});

# 3 (eksplisit)
values.clear();
await result = ajax();
values.addAll(result);
selectedValue = null;
```
*/

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Belajar2 extends StatefulWidget {
  final moduleTitle = 'Belajar 2';

  Belajar2({Key key}) : super(key: key);

  @override
  Belajar2State createState() => Belajar2State();
}

class Belajar2State extends State<Belajar2> {
  List albums = [];
  List photos = [];

  Map selectedPhoto;
  @override
  void initState() {
    super.initState();
    loadAlbums();
  }

  void loadAlbums() async {
    setState(() {
      albums.clear();
    });
    final httpResult = await http.get(Uri.http(
      'jsonplaceholder.typicode.com',
      'albums',
      {'_page': 1.toString()},
    ));
    setState(() {
      albums.addAll(jsonDecode(httpResult.body));
    });
  }

  void loadPhotos(albumId) async {
    setState(() {
      photos.clear();
    });
    final httpResult = await http.get(Uri.http(
      'jsonplaceholder.typicode.com',
      'photos',
      {
        '_page': 1.toString(),
        'albumId': albumId.toString(),
      },
    ));
    setState(() {
      photos.addAll(jsonDecode(httpResult.body));
      selectedPhoto = null; // MUST
    });
  }

  Widget dropdownAlbums() {
    return DropdownButtonFormField(
      onChanged: (data) {
        loadPhotos(data['id']);
      },
      decoration: InputDecoration(labelText: 'album ...'),
      isExpanded: true,
      isDense: true,
      items: albums
          .map(
            (i) => DropdownMenuItem(
              child: Text(i['title']),
              value: i,
            ),
          )
          .toList(),
    );
  }

  Widget dropdownPhotos() {
    return DropdownButtonFormField(
      onChanged: (data) {
        setState(() {
          selectedPhoto = data; // MUST
        });
      },
      decoration: InputDecoration(labelText: 'photos ...'),
      isExpanded: true,
      isDense: true,
      value: selectedPhoto,
      // value: photos.isNotEmpty ? photos[0] : null, // WORK, tapi tidak setimpal //
      items: photos
          .map(
            (i) => DropdownMenuItem(
              child: Text(i['title']),
              value: i,
            ),
          )
          .toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    EdgeInsets _padding = EdgeInsets.symmetric(horizontal: 12, vertical: 6);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
      ),
      body: Form(
        child: Column(
          children: [
            Padding(padding: _padding, child: dropdownAlbums()),
            Padding(padding: _padding, child: dropdownPhotos()),
            SizedBox(height: 12),
            Text(selectedPhoto.toString()),
            SizedBox(height: 12),
            if (selectedPhoto != null)
              Image.network(selectedPhoto['thumbnailUrl']),
          ],
        ),
      ),
    );
  }
}
