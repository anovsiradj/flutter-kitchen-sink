import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

// https://stackoverflow.com/a/62490254
funcOnceRandom(int max, [int min = 0]) {
  return min + (new Random()).nextInt(max - min);
}

// https://stackoverflow.com/a/57034842
funcRandomColor() {
  return Colors.primaries[Random().nextInt(Colors.primaries.length)];
}

// navigasi ke laman selanjutnya
funcGotoNext1(BuildContext context, [Function callback]) {
  return Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => callback?.call(),
    ),
  );
}

// navigasi ke laman sebelumnya
funcGotoPrev1(BuildContext context, [Function callback]) {
  Navigator.pop(context, callback?.call());
}

Future<void> funcDateTimeLocaleID() async {
  await initializeDateFormatting('id-ID');
}

String funcDateTimeFormatID(dynamic dt) {
  if (dt is String) dt = DateTime.parse(dt);
  return DateFormat.yMd('id').add_jms().format(dt);
}

Function funcBlur(BuildContext context) {
  final focusNode = FocusNode();
  return () {
    FocusScope.of(context).requestFocus(focusNode);
  };
}
