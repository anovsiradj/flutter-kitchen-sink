import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CompLinkButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  const CompLinkButton({Key key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(text.toString()),
      ),
      onPressed: () => onPressed?.call(),
    );
  }
}
