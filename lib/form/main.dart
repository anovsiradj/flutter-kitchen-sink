import 'package:flutter/material.dart';
import '../comp.dart';
import '../func.dart';
import './1.dart';
import './2.dart';
import './data_binding.dart';

class FormMain extends StatelessWidget {
  const FormMain({Key key}) : super(key: key);

  final moduleTitle = 'Form is a Form';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: moduleTitle,
      home: Scaffold(
        appBar: AppBar(title: Text(moduleTitle)),
        body: ListView(
          padding: EdgeInsets.all(12),
          children: [
            CompLinkButton(
              text: '/form/1',
              onPressed: () => funcGotoNext1(context, () => FormTest1()),
            ),
            CompLinkButton(
              text: '/form/2',
              onPressed: () => funcGotoNext1(context, () => FormTest2()),
            ),
            CompLinkButton(
              text: '/form/data_binding',
              onPressed: () => funcGotoNext1(context, () => DataBindingTest1()),
            ),
          ],
        ),
      ),
    );
  }
}
