/*
membuat form dengan tmapilan menarik dan interaktif.
ruang lingkup harus ada aksi create,update,delete dalam 1 route. 
TODOs: 
- tanggal digawe nggo datepicker
- rincian create
- rincian update
- rincian delete
*/

import 'package:flutter/material.dart';
import '../coms/form_input.dart';
import '../models/transaksi.dart';
import '../coms/show_dialog.dart';

class FormTest2 extends StatefulWidget {
  FormTest2({Key key}) : super(key: key);

  @override
  _FormTest2State createState() => _FormTest2State();
}

class _FormTest2State extends State<FormTest2> {
  final _formKey = GlobalKey<FormState>();
  // final _transGroup = ModelTransaksi();
  var _transaksi = <ModelTransaksi>[
    ModelTransaksi(nilaiJumlah: 2000, catatan: 'korek api gas'),
    ModelTransaksi(nilaiJumlah: 2000, catatan: 'korek api gas'),
    ModelTransaksi(nilaiJumlah: 2000, catatan: 'korek api gas'),
    ModelTransaksi(nilaiJumlah: 10000, catatan: 'rokok wismilak kretek'),
    ModelTransaksi(nilaiJumlah: 10000, catatan: 'rokok wismilak kretek'),
    ModelTransaksi(nilaiJumlah: 10000, catatan: 'rokok wismilak kretek'),
  ];

  @override
  void initState() {
    print(_transaksi[1].nilaiJumlah);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('form_test_2'),
              ElevatedButton(
                child: Text('Simpan'),
                onPressed: () {
                  comShowAlertDialog(
                    context,
                    Center(child: Text('Unimplemented')),
                  );
                },
              ),
            ],
          ),
        ),
        body: _makeForm(context),
      ),
    );
  }

  Widget _makeForm(BuildContext context) {
    const _gap = EdgeInsets.symmetric(horizontal: 12);
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Padding(padding: _gap, child: comFormInputText('Nama Transaksi')),
          Padding(padding: _gap, child: comFormInputText('Tanggal Transaksi')),
          Padding(
            padding: _gap,
            child: comFormInputSelectArray(
              'Jenis Transaksi',
              ModelTransaksi.JENIS_TRANSAKSI,
              (val) => setState(() => null),
            ),
          ),
          SizedBox(height: 24),
          Expanded(child: _makeRincian(context)),
        ],
      ),
    );
  }

  Widget _makeRincian(BuildContext context) {
    return ListView.builder(
      itemCount: _transaksi.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            title: Text(_transaksi[index].nilaiJumlah.toString()),
            subtitle: Text(_transaksi[index].catatan),
            trailing: Icon(Icons.more_vert),
          ),
        );
      },
    );
  }
}
