/* contoh implementasi data binding. untuk mengetahui cara kerja wysiwyg dan
mengetahu cara kerja reativitas object, terutama untuk form */

import 'package:flutter/material.dart';
import 'package:flutter_ks/coms/blur.dart';
import '../func.dart';

class DataBindingTest1 extends StatefulWidget {
  final moduleTitle = '/form/data_binding';

  @override
  _DataBindingTest1State createState() => _DataBindingTest1State();
}

class _DataBindingTest1State extends State<DataBindingTest1> {
  int _counter = funcOnceRandom(1996);

  String _message1 = 'what you see is what you get';
  final _message2 = TextEditingController(text: 'what you see is what you get');

  @override
  void initState() {
    super.initState();

    // untuk trigger perubahan message2.text
    _message2.addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    _message2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.moduleTitle)),
      body: ComsBlur(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('posisi angka $_counter'),
                IconButton(
                    tooltip: 'Naikin',
                    icon: Icon(Icons.arrow_circle_up),
                    onPressed: () => setState(() => _counter++)),
                IconButton(
                    tooltip: 'Turunin',
                    icon: Icon(Icons.arrow_circle_down),
                    onPressed: () => setState(() => _counter--)),
              ],
            ),
            TextField(
              onChanged: (val) => setState(() => _message1 = val),
              decoration: InputDecoration(labelText: 'wysiwyg metode manual'),
            ),
            Text('$_message1'),
            TextField(
              controller: _message2,
              decoration:
                  InputDecoration(labelText: 'wysiwyg metode controller'),
            ),
            Text('${_message2.text}'),
          ],
        ),
      ),
    );
  }
}
