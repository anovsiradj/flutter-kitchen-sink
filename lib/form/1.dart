import 'package:flutter/material.dart';

import '../models/user.dart';
import '../coms/show_dialog.dart';

class FormTest1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("/form_test_1"),
      ),
      body: Padding(
        padding: EdgeInsets.all(12),
        child: FormTest1Login(),
      ),
    );
  }
}

class FormTest1Login extends StatefulWidget {
  FormTest1Login({Key key}) : super(key: key);
  @override
  _FormTest1LoginState createState() => _FormTest1LoginState();
}

class _FormTest1LoginState extends State<FormTest1Login> {
  final _formKey = GlobalKey<FormState>();
  final _user = ModelUser();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text('contoh form sederhana untuk login (akses admin:admin).'),
          TextFormField(
            decoration: InputDecoration(labelText: 'username'),
            validator: (String val) {
              if (val.isEmpty) return 'username tidak boleh kosong';
              if (val.length <= ModelUser.LOGIN_CHAR_LENGTH)
                return 'username terlalu pendek';
              return null;
            },
            onSaved: (String val) => setState(() => _user.usr = val),
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'password'),
            validator: (String val) {
              if (val.isEmpty) return 'password tidak boleh kosong';
              if (val.length <= ModelUser.LOGIN_CHAR_LENGTH)
                return 'password terlalu pendek';
              return null;
            },
            onSaved: (String val) => setState(() => _user.pwd = val),
          ),
          ElevatedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  _onSubmit(context);
                }
              },
              child: Text('Login')),
        ],
      ),
    );
  }

  void _onSubmit(BuildContext context) {
    // validasi,manipulasi,konfirmasi
    if (_user.usr == 'admin' && _user.pwd == 'admin') {
      comShowAlertDialog(context, 'selamat datang');
    } else {
      comShowAlertDialog(
          context, 'akun tidak ditemukan, username atau password salah.');
    }
  }
}
