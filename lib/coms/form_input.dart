import 'package:flutter/material.dart';

/* template kabeh input elements nggo form */

comFormInputText(String label) {
  return TextFormField(
    decoration: InputDecoration(
      labelText: label,
    ),
  );
}

comFormInputSelectArray(
    String label, List<String> options, Function(String) onChangedCallback) {
  return DropdownButtonFormField(
    decoration: InputDecoration(labelText: label),
    items: options
        .map((val) => DropdownMenuItem(
              value: val.toString(),
              child: Text(val),
            ))
        .toList(),
    onChanged: onChangedCallback,
  );
}
