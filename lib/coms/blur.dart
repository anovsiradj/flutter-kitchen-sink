import 'package:flutter/material.dart';

class ComsBlur extends StatelessWidget {
  /// widget ini tujuannya digunakan sebagai parent dari form,
  /// supaya bisa blur semua input ketika ...
  /// <https://flutterigniter.com/dismiss-keyboard-form-lose-focus/>
  ComsBlur({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: child,
      onTap: () {
        final currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
      },
    );
  }
}
