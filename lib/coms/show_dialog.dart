import 'package:flutter/material.dart';

Future<void> comShowAlertDialog(BuildContext context, dynamic content,
    [Function(BuildContext) actionsCallback]) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: actionsCallback == null,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        content: SingleChildScrollView(
          child: (content is String) ? Text(content) : content,
        ),
        // metode callback, supaya pake context dari dialog, bukan caller.
        actions: actionsCallback?.call(dialogContext),
      );
    },
  );
}
