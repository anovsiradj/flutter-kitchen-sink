import 'package:flutter/material.dart';
import '../func.dart';
import './stateful_widget.dart';
import './stateless_widget.dart';

class TempatesMain extends StatelessWidget {
  final moduleTitle = 'Templates';

  const TempatesMain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: moduleTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(moduleTitle),
        ),
        body: ListView(
          children: [
            TextButton(
              child: Text('StatefulModule'),
              onPressed: () => funcGotoNext1(context, () => StatefulModule()),
            ),
            TextButton(
              child: Text('StatelessModule'),
              onPressed: () => funcGotoNext1(context, () => StatelessModule()),
            ),
          ],
        ),
      ),
    );
  }
}
