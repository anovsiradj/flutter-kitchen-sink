import 'package:flutter/material.dart';

class StatefulModule extends StatefulWidget {
  final moduleTitle = 'moduleTitle';

  StatefulModule({Key key}) : super(key: key);

  @override
  StatefulModuleState createState() => StatefulModuleState();
}

class StatefulModuleState extends State<StatefulModule> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.moduleTitle),
      ),
      body: Center(
        child: Text(widget.moduleTitle),
      ),
    );
  }
}
