import 'package:flutter/material.dart';

class StatelessModule extends StatelessWidget {
  final moduleTitle = 'moduleTitle';

  const StatelessModule({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(moduleTitle),
      ),
      body: Center(
        child: Text(moduleTitle),
      ),
    );
  }
}
