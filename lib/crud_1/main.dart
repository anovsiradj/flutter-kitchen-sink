import 'package:flutter/material.dart';
import './list.dart';

class CRUD1App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'crud_1',
      home: CRUD1List(),
    );
  }
}
