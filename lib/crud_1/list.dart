import 'package:flutter/material.dart';
import '../func.dart';
import '../coms/show_dialog.dart';
import './form.dart';
import './item.dart';

class CRUD1List extends StatefulWidget {
  CRUD1List({Key key}) : super(key: key);

  @override
  _CRUD1ListState createState() => _CRUD1ListState();
}

class _CRUD1ListState extends State<CRUD1List> {
  // OTW: data ganti dari static array jadi ajax,database
  List<String> contents = [
    'lorem ipsum',
    'hello world',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('/crud_1/list'),
      ),
      body: ListView.builder(
        itemCount: contents.length,
        itemBuilder: _viewItem,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _createAction(context);
        },
      ),
    );
  }

  Widget _viewItem(BuildContext context, int i) {
    return ListTile(
      title: Text(contents[i]),
      trailing: Wrap(
        // spacing: 12,
        children: [
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                _deleteAction(context, i);
              }),
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              _updateAction(context, i);
            },
          ),
        ],
      ),
      onTap: () {
        funcGotoNext1(context, () => CRUD1Item(content: contents[i]));
      },
    );
  }

  _deleteAction(BuildContext context, int i) async {
    await comShowAlertDialog(context, 'Yakin?', (BuildContext dialogContext) {
      return <Widget>[
        TextButton(
          child: Text('Tidak'),
          onPressed: () {
            funcGotoPrev1(dialogContext, () {});
          },
        ),
        TextButton(
          child: Text('Ya'),
          onPressed: () {
            funcGotoPrev1(
                dialogContext, () => setState(() => contents.removeAt(i)));
          },
        ),
      ];
    });
  }

  _createAction(BuildContext context) async {
    final Map result = await funcGotoNext1(context, () => CRUD1Form(index: -1));
    setState(() => contents.add(result['content']));
  }

  _updateAction(BuildContext context, int index) async {
    final Map result = await funcGotoNext1(
      context,
      () => CRUD1Form(index: index, content: contents[index]),
    );
    setState(() => contents[index] = result['content']);
  }
}
