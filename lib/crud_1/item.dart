import 'package:flutter/material.dart';

class CRUD1Item extends StatelessWidget {
  final String content;
  CRUD1Item({Key key, this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('/crud_1/item'),
      ),
      body: Padding(
        padding: EdgeInsets.all(12),
        child: Text(content),
      ),
    );
  }
}
