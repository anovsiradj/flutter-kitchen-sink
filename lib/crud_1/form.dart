import 'package:flutter/material.dart';
import '../func.dart';

class CRUD1Form extends StatefulWidget {
  final int index;
  final String content;
  CRUD1Form({Key key, @required this.index, this.content}) : super(key: key);

  @override
  _CRUD1FormState createState() => _CRUD1FormState();
}

class _CRUD1FormState extends State<CRUD1Form> {
  final _formKey = GlobalKey<FormState>();
  final _content = TextEditingController();

  @override
  void initState() {
    if (widget.content != null) _content.text = widget.content;
    _content.addListener(() => setState(() {}));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('/crud_1/form'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: _content,
              validator: (String val) {
                if (val.trim().length == 0) return 'tidak boleh kosong';
                return null;
              },
            ),
            ElevatedButton(
              child: Text('simpan'),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  funcGotoPrev1(context,
                      () => {'index': widget.index, 'content': _content.text});
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
