import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:flutter_markdown/flutter_markdown.dart';
import './form/main.dart';
import './crud_1/main.dart';
import './helloworld.dart';
import './belajar/main.dart';
import './templates/main.dart';
import './jaringan/main.dart';
import './samples/main.dart';
import 'comp.dart';
import 'firebase/main.dart';
import 'multimedia/main.dart';

Future<void> main() async {
  await DotEnv.load();

  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/': (context) => Homepage(),
      '/form': (context) => FormMain(),
      '/crud_1': (context) => CRUD1App(),
      '/helloworld': (context) => HelloWorld(),
      '/firebase': (_) => MyFirebaseMain(),
      '/belajar': (context) => BelajarMain(),
      '/templates': (context) => TempatesMain(),
      '/jaringan': (context) => JaringanMain(),
      '/samples': (context) => SamplesMain(),
      '/multimedia': (_) => MultimediaMain(),
    },
  ));
}

class Homepage extends StatelessWidget {
  const Homepage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final readmeWidget = FutureBuilder(
      future: rootBundle.loadString('README.md', cache: true),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        if (snapshot.hasData) {
          return MarkdownBody(data: snapshot.data);
        }
        return Center(child: CircularProgressIndicator(value: null));
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(DotEnv.env['APP_NAME']),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(12),
        child: Column(
          children: <Widget>[
            Container(
              child: readmeWidget,
              padding: EdgeInsets.only(bottom: 16),
            ),
            _link(context, '/helloworld'),
            _link(context, '/multimedia'),
            _link(context, '/form'),
            _link(context, '/crud_1'),
            _link(context, '/bloc_1'),
            _link(context, '/firebase', 'Firebase (Google)'),
            _link(context, '/belajar'),
            _link(context, '/templates'),
            _link(context, '/jaringan'),
            _link(context, '/samples'),
          ],
        ),
      ),
    );
  }

  Widget _link(BuildContext context, String href, [String text]) {
    return CompLinkButton(
      text: text ?? href,
      onPressed: () => Navigator.pushNamed(context, href),
    );
  }
}
